#!/bin/sh

if [ ! -d /mail ]; then
	echo "You must mount your watch folder at /mail" >&2
	exit 1
fi

adduser -u $LOCAL_USER_ID -D user

DB="/notmuch-config"
if [ ! -f $DB ]; then
	  echo "You must mount your notmuch config at $DB" >&2
	  exit 1
fi

if [ "$(stat -c "%s" $DB)" -eq 0 ]; then
    su-exec user notmuch setup
    cat /home/user/.notmuch-config > /notmuch-config
    exit
fi

sed -E 's|^path *=.*$|path=/mail|' /notmuch-config > /home/user/.notmuch-config
chown user /home/user/.notmuch-config

if [ -n "$(which $1)" ]; then
    su-exec user "$@"
else
    su-exec user notmuch "$@"
fi
