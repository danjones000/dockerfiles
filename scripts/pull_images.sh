#!/bin/sh

cd "$(dirname "$0")/.." || exit

tag=${1:-latest}

bin=docker
if command -v podman >/dev/null; then
    bin=podman
fi

echo "Pulling with $bin"

for image in *; do
	if [ -f "$image/Dockerfile" ]; then
      $bin pull "registry.gitlab.com/danjones000/dockerfiles/$image:$tag" || exit
	fi
done
