#!/bin/sh

cd "$(dirname "$0")/.." || exit

current=$1
new=${2:-latest}

bin=docker
if command -v podman >/dev/null; then
    bin=podman
fi

echo "Tagging with $bin"

for image in *; do
	if [ -f "$image/Dockerfile" ]; then
		  $bin tag "registry.gitlab.com/danjones000/dockerfiles/$image:$current" "registry.gitlab.com/danjones000/dockerfiles/$image:$new" || exit
	fi
done
