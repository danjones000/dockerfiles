#!/bin/sh

if [ ! -d /watch ]; then
	echo "You must mount your watch folder at /watch" >&2
	exit 1
fi

DB="/root/.flexget/db-config.sqlite"
if [ ! -f $DB ]; then
	echo "You must mount your flexget config at $DB" >&2
	exit 1
fi

mkdir -p /root/.flexget
cp /etc/flexget.yml /root/.flexget/config.yml

if [ -n "$(which $1)" ]; then
    "$@"
elif [ "$1" = "get-crawljob" ]; then
	shift
	flexget seen add "$1"
	crawljob.sh -a "$@"
else
	flexget "$@"
fi
