#!/bin/sh

auto=false

if [ "$1" = "-a" ]; then
    auto=true
    shift
fi

url="$1"
title="$2"

if [ -z "$url" ]; then
	echo "No URL specified" >&2
	exit 1
fi

trail="${url%/}"
slug="${trail##*/}"

if [ -z "$slug" ]; then
	slug=$(date -Is | tr ':' -)
fi

echo "Adding $url to crawljob"

(
    echo "# $title "
    echo "text = $url"
    if [ -n "$DL_FOLDER" ]; then
	      echo "downloadFolder = $DL_FOLDER"
    fi
    if [ $auto = true ]; then
        echo "enabled = true"
        echo "autoStart = true"
        echo "autoConfirm = true"
    fi
) > /watch/$slug.crawljob
